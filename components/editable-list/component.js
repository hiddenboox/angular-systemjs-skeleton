'use strict';

import {Component, template} from '../../ng-decorators';
import htmlTemplate from './template.html!text';
import Controller from './controller';


@Component('editableList')
class App {
    constructor() {
        this.template = htmlTemplate;
        this.controller = Controller;
        this.controllerAs = 'vm';
        this.bindings = {
            airplanes: '<',
            onEdit: '&'
        };
    }
}
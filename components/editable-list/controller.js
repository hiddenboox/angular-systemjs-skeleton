'use strict';

export default class Controller {
    constructor($timeout, breezeService) {
        this.entities = [];
        this.$timeout = $timeout;
    }

    $onInit() {
        this.airplanes
        .then(res => {
            res.results.forEach(entity => {
                entity.entityAspect.propertyChanged.subscribe(args => {
                    this.onEdit({entity: args.entity});
                });
                this.entities.push(entity);
            });
        });
    }

    edit(entity) {
        this.onEdit({ entity });
    }
}
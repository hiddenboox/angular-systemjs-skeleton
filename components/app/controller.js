'use strict';

import breeze from 'breeze';

const BREEZE_SERVICE = new WeakMap();

export default class Controller {
    constructor(breezeService) {
        this.breezeService = breezeService;
    }

    $onInit() {
        this.airplanes = new Promise(resolve => {
            this.breezeService.fetchMetadata().then(() => {
                this.fetchAirplanes().then(resolve);
            });
        });
    }

    _resolve(resolved) {
        return resolved.results;
    }

    edit(entity) {
        console.log(entity.registrationMark);
    }

    fetchAirplanes() {
        const query = new breeze.EntityQuery('Airplanes/Airplanes');

        return this.breezeService.entityManager.executeQuery(query);
    }
}
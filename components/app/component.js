'use strict';

import {Component, template} from '../../ng-decorators';
import htmlTemplate from './template.html!text';
import Controller from './controller';


@Component('app')
class App {
    constructor() {
        this.controller = Controller;
        this.template = htmlTemplate;
        this.bindings = {
            title: '@'
        };
    }
}
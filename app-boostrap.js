import angular from 'angular';
import {app} from 'app';

import './app-initializer';

angular.element(document).ready(() => {
   angular.bootstrap(document.getElementById('shell'), [app.name]);
});

'use strict';

import {Service} from '../ng-decorators';
import breeze from 'breeze';

@Service('breezeService')
class BreezeService {
    constructor($window) {
        this.$window = $window;
        this.breeze = breeze;
        this.dataService = new breeze.DataService({
            serviceName: 'http://localhost:4000/api/'
        });

        this.metadataStore = new breeze.MetadataStore({
            namingConvention: breeze.NamingConvention.camelCase
        });

        this.entityManager = new breeze.EntityManager({
            dataService: this.dataService,
            metadataStore: this.metadataStore
        });
    }

    fetchMetadata(willCache) {
        let metadataAsString = this.$window.localStorage.getItem('metadataStore');

        if (metadataAsString) {
            this.entityManager.metadataStore.importMetadata(metadataAsString, true);
            return Promise.resolve();
        } else {
            return this.entityManager.fetchMetadata()
                .then(() => {
                    metadataAsString = this.entityManager.metadataStore.exportMetadata();
                    window.localStorage.setItem('metadataStore', metadataAsString);
                });
        }
    }
}
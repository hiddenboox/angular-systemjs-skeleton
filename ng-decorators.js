'use strict';

import {app} from 'app';
import fetchTemplate from 'fetch-template';

export function Component(name) {
    return function decorator(target) {
        let componentInstance = {};

        if(target.$template) {
            target.$template().then(res => {
                componentInstance.template = res;
            });
        }
        app.component(name, Object.assign({}, new target(), componentInstance));
    }
}

export function Service(name) {
    return function decorator(target) {
        app.service(name, target);
    }
}

export function Run() {
    return function decorator(target, key, descriptor) {
        app.run(descriptor.value);
    }
}
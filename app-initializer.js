'use strict';

// components
import 'components/app/component';
import 'components/editable-list/component';

// runs
import 'runs/attach-main-to-dom';
import 'runs/configure-breeze-adapter';

//services
import 'services/breeze';
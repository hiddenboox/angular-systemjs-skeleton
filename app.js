import angular from 'angular';
import dependencies from 'app-dependencies';

export const app = angular.module('app', dependencies);
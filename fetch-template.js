'use strict';

module.exports = name => {
    return require(`${name}.html`);
};
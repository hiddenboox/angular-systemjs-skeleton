'use strict';

import {Run} from '../ng-decorators';

class OnRun {
    @Run()
    static run() {
        const [appDOM] = document.getElementsByTagName('app');

        if (!appDOM) {
            document.getElementById('shell').appendChild(document.createElement('app'));
        }
    }
}